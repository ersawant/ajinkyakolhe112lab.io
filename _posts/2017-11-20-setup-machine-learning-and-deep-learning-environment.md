---
layout: post
title:  "Setup your Environment for Machine Learning & Deep Learning"
description: Setup your local and cloud environment to do machine learning and deep learning.  
date:   2017-11-20 14:10:51 +0800
categories: ['blog']
tags: ['Machine Learning', 'Deep Learning']
---
# Install the following

1. Install Anaconda Python 3.6 version
    1. Launch Anaconda Prompt
    2. Execute the following commands
    ```
    conda update --all -c conda-forge -y
    conda install mlxtend -c conda-forge -y
    conda install gensim -c conda-forge -y
    ```
2. Install [Nteract Desktop Application for Jupyter Notebooks](https://nteract.io/)
3. Create account on following websites
    1. [Azure Notebooks](https://notebooks.azure.com): Web IDE for doing Machine Learning on Cloud
    2. [Register on Google Colaboratory](https://research.google.com/colaboratory): Shared Jupyter Notebook Environment to work collaboratively
    3. [Kaggle](https://www.kaggle.com/)

## Extra Tools
1. Install [Git](https://git-scm.com/)
