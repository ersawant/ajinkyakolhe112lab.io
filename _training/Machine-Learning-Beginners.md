---
layout: training
title: Machine Learning For Beginners
description: Machine Learning Training for Beginners
---

Learn to use Pandas, Matplotlib and Scikit-learn to build predictive models on real-world data.

The workshop is meant to develop a solid base to build your machine learning skills. In particular you will learn to:

- Recognize problems that can be solved with Machine Learning
- Select the right technique (Classification, Regression? Clustering, Preprocessing?)
- Load and manipulate data with Pandas
- Visualize and explore data with Matplotlib
- Build regression, classification and clustering models with Scikit-Learn
- Evaluate model performance with Scikit-Learn
- Build, train and serve a predictive model using Python


## Curriculum

1. **Environment Setup**
  - Setting up Anaconda, Pycharm & Jupyter Notebook
  - Quick overview of Python

2. **Theoretical Foundation of Machine Learning**
  - Overview of Machine Learning
    - Supervised Learning, Unsupervised Learning
    - Classification, Clustering & Regression
  - Phases of ML Project
    1. Data Collection
    2. Data Visualization
    3. Data Preprocessing & Cleanup
    4. Model Creation
    5. Evaluation and Fine Tuning

3. **Phase 1: Data Collection**
  - BeautifulSoup & Scrappy for web collection
  - Twitter, linkedin, facebook api's for data collection

4. **Phase 2: Data Visualization / EDA using Matplotlib**
  - Using matplotlib & Seaborn for Data visualization for inference
  - Correlation doesn't mean causation

5. **Phase 3: Data Preprocessing & Cleanup using Pandas**
  - Using Pandas for Data Preprocessing, Cleaning dataset, filling missing values, normalization
  - **Feature engineering**

6. **Phase 4: Model Creation using Scikit-learn**
  - Choosing features for ml model using scikit-learn
  - Model creation & evaluation

7. **Phase 5: Accuracy & Fine Tuning**
  - Choosing Accuracy metric
  - Cross Validation, Regularization
  - Fine Tuning the model by feature engineering
  - Fine Tuning the model by hyper parameter optimization
  - Model Ensemble
  - Dimensionality Reduction
