1. If on windows, Install [Bash on Windows](https://msdn.microsoft.com/en-us/commandline/wsl/install_guide)
2. Install following in Bash `sudo apt-get install git ruby ruby-dev build-essential`
3. If on windows, Path to windows drives is `/mnt/`
4. `git clone <PATH>/https://github.com/ajinkyakolhe112/ajinkyakolhe112.github.io`
5. `cd <PATH>/ajinkyakolhe112.github.io`
6. Install bundler with `sudo gem install bundler`
7. Install gems with `bundler install`
8. Configure your site settings using the `_config.yml`
9. See website locally with `bundle exec jekyll build && bundle exec jekyll serve --baseurl=""`
